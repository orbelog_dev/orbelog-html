var AcompanhamentoHandler = function() {

    var handleGrafic = function() {

        $.ajax({
            url: "data/acompanhamento.json",
            dataType: "json",
            success: function(data){
                
                console.log(data);
                
                $('#highchart').highcharts({
                    chart : {
                        type: 'column'
                    },
                    title: {
                        text: 'Acompanhamento de Entregas',
                        x: -20 //center
                    },
                    subtitle: {
                        text: '(Atualizado em 19/10/2016 15:15)',
                        x: -20
                    },
                    xAxis: {
                        categories: data.xAxis.data
                    },
                    yAxis: [
                        {
                            min:0,
                            title: {
                                text: 'Viagens do dia'
                            }
                        }
                    ],
                    tooltip: {
                        shared: true
                    },
                    plotOptions:{
                        column:{
                            grouping: false,
                            shadow: false,
                            borderWidth: 0
                        }
                    },
                    series: [data.serie1, 
                             data.serie2]
                });
            },
            error: function(err){
                console.log(err);
            },
            complete: function(){

            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleGrafic();
        }
    };

}();


var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#sample_1');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": ative para ordenar a coluna de forma crescente",
                    "sortDescending": ": ative para ordenar a coluna de forma decrescente"
                },
                "emptyTable": "Não tem nenhuma informação para ser exibida",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhuma informação retornada",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "lengthMenu": "Exibir _MENU_",
                "search": "Busca:",
                "zeroRecords": "Nenhum registro encontrado",
                "paginate": {
                    "previous":"Anterior",
                    "next": "Próximo",
                    "last": "Último",
                    "first": "Primeiro"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,     
            "scrollX": true,       
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_1_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
            
        }

    };

}();




jQuery(document).ready(function() {
    AcompanhamentoHandler.init();
    TableDatatablesManaged.init();
});

