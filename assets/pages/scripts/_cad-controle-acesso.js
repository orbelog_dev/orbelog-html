jQuery(document).ready(function() {
    $("#btn-cancelar").click(function(){
        window.location.href = "list-usuario.html";
    });     

    $("#cmbFilial").on("select2:select", function(){
        $(this).closest("form").submit();
    });

    $(".chosen-select").chosen();

    $("#title-collapse").click(function(e){
        var $this = $(this);
        var target = $('#portlet-body-search');
        var $icon = $("#icon-collapse");

        target.slideToggle(1000, 'easeOutCubic', function(){
            if (target.is(":visible")){
                $icon.removeClass("fa-chevron-up").addClass("fa-chevron-down");
            }else{
                $icon.removeClass("fa-chevron-down").addClass("fa-chevron-up");
            }
        });
    });

    $("#icon-collapse").click(function(e){
        var $this = $(this);
        var target = $('#portlet-body-search');
        
        target.slideToggle(1000, 'easeOutCubic', function(){
            if (target.is(":visible")){
                $this.removeClass("fa-chevron-up").addClass("fa-chevron-down");
            }else{
                $this.removeClass("fa-chevron-down").addClass("fa-chevron-up");
            }
        });
    });
    

    $("#btn-buscar").click(function(e){
        $("#pnPesquisa").show().delay(500); 
        App.scrollTo($("#pnPesquisa"), -50);       
    });



});