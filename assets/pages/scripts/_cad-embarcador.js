var FormValidation = function () {

    // basic validation
    var handleValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form = $('#form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    nome: {
                        required: true
                    },
                    nmfantasia: {
                        required: true
                    },
                    cnpj: {
                        required: true,
                        cnpjBR: true
                    },
                    ie: {
                        required: true,
                        ieBR: true
                    },
                    im:{
                        imBR: true
                    },
                    status: {
                        required: true
                    },
                    inss: {
                        required: true
                    },
                    nomecontato:{
                        required: true
                    },
                    cargo:{
                        required: true
                    },
                    telFixo:{
                        required: true
                    },
                    telCelular:{
                        required: true
                    },
                    tipocontato: {
                        required: true
                    },
                    operadora: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    status: {
                        required: true
                    },
                    cep:{
                        required: true
                    },
                    endereco:{
                        required: true
                    },
                    numero:{
                        required: true
                    },
                    uf:{
                        required: true
                    },
                    cidade:{
                        required: true
                    },
                    bairro:{
                        required: true
                    },
                    tipoendereco:{
                        required: true
                    },
                    radioORBE:{
                        required: true
                    },
                    tipoimovel:{
                        required: true
                    }

                },
                messages: {
                    nome: {
                        required: " "
                    },
                    nmfantasia: {
                        required: ""
                    },
                    cnpj: {
                        required: "CNPJ Obrigatório",
                        cnpjBR: "CNPJ Inválido"
                    },
                    ie: {
                        required: "IE Obrigatória",
                        ieBR: "IE Inválida / Deve ter 12 Digitos ou ser isento"
                    },
                    im:{
                        imBR: "IM Inválida"
                    },
                    inss: {
                        required: ""
                    },
                    nomecontato:{
                        required: ""
                    },
                    cargo:{
                        required: ""
                    },
                    telFixo:{
                        required: ""
                    },
                    email: {
                        required: "Email Obrigatório",
                        email: "Email Invalido"
                    },
                    cep:{
                        required: ""
                    },
                    endereco:{
                        required: ""
                    },
                    numero:{
                        required: ""
                    },
                    uf:{
                        required: ""
                    },
                    cidade:{
                        required: ""
                    },
                    bairro:{
                        required: ""
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if(element.attr("name") == "usage_terms") {
                    error.appendTo( element.parent("div").next("div") );
                    } else {
                    error.insertAfter(element);
                    }
                },

                highlight: function (element) { // hightlight error inputs
                    $("#" + $(element).data('label')).addClass('has-error');
                    $(element).addClass('has-error');
                    $(element).parent().find('.chosen-single').addClass('has-error');
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $("#" + $(element).data('label')).removeClass('has-error');
                    $(element).removeClass('has-error');
                    $(element).parent().find('.chosen-single').removeClass('has-error');
                },

                success: function (label) {
                    
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    App.scrollTo(error, -200);
                }
            });

    }

    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"],
                "html" : false,
                
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {

            handleWysihtml5();
            handleValidation();
            
            $("#btn-cancelar").click(function(){
                window.location.href = "list-pesquisa-embarcador.html";
            });     

            $("#cmbFilial").on("select2:select", function(){
                $(this).closest("form").submit();
            });


            $(".chosen-select").chosen();

            // Instance the tour
            var tour = new Tour({
              backdrop: true,
              template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'>Titulo do passo 1</h3><div class='popover-content'>Incluir os dados da ajuda </div><div class='popover-navigation text-center'><button class='btn btn-default' data-role='prev'>« Anterior</button><span data-role='separator'>&nbsp;</span><button class='btn btn-default' data-role='next'>Próximo »</button></div><div class='popover-navigation'><div class=' text-center'><button class='btn btn-primary' data-role='end'>Finalizar</button></div></div></div>",
              steps: [
              {
                element: "#form-cadastro",
                content: "<p>Dentro deste formulário, você colocará os dados do usuário",
                title: "titulo da ajuda",
                placement: "top"
              },
              {
                element: "#form-table",
                content: "<p>Nesta etapa será definida os dados do usuário</p>",
                title: "titulo da ajuda 2",
                placement: "top"
              }
            ]});

            tour.init();

            $("#bt-tour").click(function(){
                // Start the tour
                tour.start(true);    
            });         

            $('.date-picker').datepicker();
            $(".date-picker input").inputmask("d/m/y", {showMaskOnHover : false});
            $("#upload").fileinput({'showUpload':false, 'showPreview': true, 'language': 'pt-BR'});
            $("#cpf").inputmask("999.999.999-99", {placeholder:"_", showMaskOnHover : false});
            $(".mask-cnpj").inputmask("99.999.999/9999-99", {showMaskOnHover : false});
            $(".mask-telefone").inputmask("+55 (99) 9999-9999", {showMaskOnHover : false});
            $("#cep").inputmask("99999-999", {showMaskOnHover : false});
            $(".inscricao").inputmask("*{0,12}", {showMaskOnHover : false});
            $("#numero").inputmask("9{0,5}", {showMaskOnHover : false});
            $(".mask-telefone").on("keydown change blur", function(e) {
                var isNumber = (((e.keyCode >= 48) && (e.keyCode <= 57)) || ((e.keyCode >= 96) && (e.keyCode <= 105)));
                var isTab = (e.keyCode == 9);
                var isRemoveKey = ((e.keyCode == 8) || (e.keyCode == 46));

                if ((!isTab && isNumber) || isRemoveKey){
                    if ($(this).val().replace(/[_\-()]/g,"").length >= 15){
                        $(this).inputmask("+55 (99) 99999-9999", {showMaskOnHover : false});
                    }else {
                        $(this).inputmask("+55 (99) 9999-9999", {showMaskOnHover : false});
                    }
                }
            });


        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
    TableDatatablesManaged.init();

});
var TableDatatablesManaged = function () {

    var table = "";

    var initTable1 = function () {
        table = $('#sample_1');
        var btnExportar =   '<div class="btn-group">'+
                            '    <button class="btn green  dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Exportar'+
                            '        <i class="fa fa-angle-down"></i>'+
                            '    </button>'+
                            '    <ul class="dropdown-menu pull-right">'+
                            '        <li>'+
                            '            <a href="javascript:;">'+
                            '                <i class="fa fa-file-pdf-o"></i> Exportar para PDF </a>'+
                            '        </li>'+
                            '        <li>'+
                            '            <a href="javascript:;">'+
                            '                <i class="fa fa-file-excel-o"></i> Exportar para Excel </a>'+
                            '        </li>'+
                            '    </ul>'+
                            '</div>';

        var btnAcoes =  '<div class="btn-group">' +
                        '    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações' +
                        '        <i class="fa fa-angle-down"></i>' +
                        '    </button>' +
                        '   <ul class="dropdown-menu pull-right" role="menu">' +
                        '       <li>' +
                        '           <a class="lnk-editar" href="javascript:;">' +
                        '                <i class="fa fa-pencil"></i> Editar / Visualizar </a>' +
                        '        </li>' +
                        '        <li>' +
                        '            <a class="lnk-desativar" href="javascript:;">' +
                        '                <i class="fa fa-toggle-on"></i> Inativar </a>' +
                        '        </li>' +
                        '        <li>' +
                        '            <a class="lnk-remover" href="javascript:;">' +
                        '                <i class="fa fa-remove"></i> Excluir' +
                        '            </a>' +
                        '        </li>' +
                        '    </ul>' +
                        '</div>';

        // begin first table
        table.dataTable({
            "ajax": "data/embarcador.json",
            "processing": true,
            "responsive": true,
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": ative para ordenar a coluna de forma crescente",
                    "sortDescending": ": ative para ordenar a coluna de forma decrescente"
                },
                "emptyTable": "Não tem nenhuma informação para ser exibida",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhuma informação retornada",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "lengthMenu": "Exibir _MENU_",
                "search": "Busca:",
                "zeroRecords": "Nenhum registro encontrado",
                "paginate": {
                    "previous":"Anterior",
                    "next": "Próximo",
                    "last": "Último",
                    "first": "Primeiro"
                }
            },

            "initComplete": function( settings, json ) {
                $(".btn-actions").html(btnExportar);

                $(".dataTables_filter label input").focus(function(){
                    $(this).removeClass('input-small').animate({width : "250px"}, 2000, 'easeOutElastic');
                });

                App.scrollTo($("#ancoraPesquisa"), -50);
            },

            "drawCallback": function( settings ) {
                $(".lnk-editar").click(function(e){
                    window.location = "#";
                });
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'<'btn-actions pull-right'><'pull-right'f>>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 15, 20],
                [10, 15, 20] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,            
            "pagingType": "bootstrap_full_number",
            //"columns": [
            //    { "data": "status" },
            //   { "data": "nome" },
            //    { "data": "sobrenome" },
            //    { "data": "email" },
            //    { "data": "dt_vigencia" },
            //    { "data": null }
            //],
            "columnDefs": [
                {  // set default column settings
                    "width": "5%",
                    "data": "status",
                    "className": "dt-center",
                    "responsivePriority": 1,
                    "targets": [0]
                },
                {  // set default column settings
                    "width": "20%",
                    "data": "prestador",
                    "responsivePriority": 2,
                    'targets': [1]
                },
                {  // set default column settings
                    "width": "20%",
                    "data": "servico",
                    "responsivePriority": 3,
                    'targets': [2]
                },
                {  // set default column settings
                    "width": "10%",
                    "data": "obrigatorio",
                    'targets': [3]
                },
                {  // set default column settings
                    "width": "20%",
                    "data": "vigencia-de",
                    'targets': [4]
                },
                {  // set default column settings
                    "width": "20%",
                    "data": "vigencia-ate",
                    'targets': [5]
                },
                {
                    "width": "5%",
                    "searchable": false,
                    "orderable": false,
                    "targets": [6],
                    "data" : null,
                    "responsivePriority": 0,
                    "defaultContent" : btnAcoes
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

    }

    var refreshTable = function(){
        App.scrollTo($("#ancoraPesquisa"), -50);
    }
    var isInitialized = function(){
        return (table != "")
    }


    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            if (isInitialized())
                refreshTable();    
            else
                initTable1();              
            
        }

    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        
        $("#title-collapse").click(function(e){
            var $this = $(this);
            var target = $('#portlet-body-search');
            var $icon = $("#icon-collapse");

            target.slideToggle(1000, 'easeOutCubic', function(){
                if (target.is(":visible")){
                    $icon.removeClass("fa-chevron-up").addClass("fa-chevron-down");
                }else{
                    $icon.removeClass("fa-chevron-down").addClass("fa-chevron-up");
                }
            });
        });

        $("#icon-collapse").click(function(e){
            var $this = $(this);
            var target = $('#portlet-body-search');
            
            target.slideToggle(1000, 'easeOutCubic', function(){
                if (target.is(":visible")){
                    $this.removeClass("fa-chevron-up").addClass("fa-chevron-down");
                }else{
                    $this.removeClass("fa-chevron-down").addClass("fa-chevron-up");
                }
            });
        });
        
        //initialize datepicker
        $('.date-picker').datepicker({
            autoclose: true
        });

        $(".chosen-select").chosen();
        $(".chosen-select.responsiveChosen").chosen({width:"100%"});

        $("#btn-pesquisa").click(function(){
            $("#pnPesquisa").show();
            TableDatatablesManaged.init();
        });
        
    });
}

