var FormValidation = function () {

    // basic validation
    var handleValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form = $('#form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    filial: {
                        required: true
                    },
                    cnpj: {
                        required: true,
                        cnpjBR: true
                    },
                    ie: {
                        required: true,
                        irBR: true
                    },
                    nomecontato: {
                        required: true
                    },
                    cargo: {
                        required: true
                    },
                    sexo: {
                        required: true
                    },
                    estadocivil: {
                        required: true
                    },
                    nomemae:{
                        required: true
                    },
                    nomepai:{
                        required: true
                    },
                    telFixo:{
                        required: true
                    },
                    telCelular:{
                        required: true
                    },
                    tipocontato: {
                        required: true
                    },
                    operadora: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    status: {
                        required: true
                    },
                    codrntrc: {
                        required: true
                    },
                    datarntrc: {
                        required: true
                    },
                    validaderntrc: {
                        required: true
                    },
                    codcnh: {
                        required: true
                    },
                    categoriacnh: {
                        required: true
                    },
                    emissaocnh: {
                        required: true
                    },
                    ufcnh: {
                        required: true
                    },
                    cidadecnh: {
                        required: true
                    },
                    primeiracnh: {
                        required: true
                    },
                    vigenciacnh: {
                        required: true
                    },
                    tipotac: {
                        required: true
                    },
                    cep:{
                        required: true
                    },
                    endereco:{
                        required: true
                    },
                    numero:{
                        required: true
                    },
                    uf:{
                        required: true
                    },
                    cidade:{
                        required: true
                    },
                    bairro:{
                        required: true
                    },
                    tipoendereco:{
                        required: true
                    },
                    tipoimovel:{
                        required: true
                    }

                },
                messages: {
                    nome: {
                        required: ""
                    },
                    nmfantasia: {
                        required: ""
                    },
                    cnpj: {
                        required: "CNPJ Obrigatório",
                        cnpjBR: "CNPJ Inválido"
                    },
                    ie: {
                        required: "IE Obrigatória",
                        ieBR: "IE Inválida / Deve ter 12 Digitos ou ser isento"
                    },
                    im:{
                        imBR: "IM Inválida"
                    },
                    inss: {
                        required: ""
                    },
                    nomecontato:{
                        required: ""
                    },
                    cargo:{
                        required: ""
                    },
                    telFixo:{
                        required: ""
                    },
                    email: {
                        required: "Email Obrigatório",
                        email: "Email Invalido"
                    },
                    cep:{
                        required: ""
                    },
                    endereco:{
                        required: ""
                    },
                    numero:{
                        required: ""
                    },
                    uf:{
                        required: ""
                    },
                    cidade:{
                        required: ""
                    },
                    bairro:{
                        required: ""
                    },
                    filial:{
                        required: ""
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if(element.attr("name") == "usage_terms") {
                    error.appendTo( element.parent("div").next("div") );
                    } else {
                    error.insertAfter(element);
                    }
                },

                highlight: function (element) { // hightlight error inputs
                    $("#" + $(element).data('label')).addClass('has-error');
                    $(element).addClass('has-error');
                    $(element).parent().find('.chosen-single').addClass('has-error');
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $("#" + $(element).data('label')).removeClass('has-error');
                    $(element).removeClass('has-error');
                    $(element).parent().find('.chosen-single').removeClass('has-error');
                },

                success: function (label) {
                    
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    App.scrollTo(error, -200);
                }
            });

    }

    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"],
                "html" : false,
                
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {

            handleWysihtml5();
            handleValidation();
            
            $("#btn-cancelar").click(function(){
                window.location.href = "list-filial.html";
            });     

            $("#cmbFilial").on("select2:select", function(){
                $(this).closest("form").submit();
            });


            $(".chosen-select").chosen();

            // Instance the tour
            var tour = new Tour({
              backdrop: true,
              template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'>Titulo do passo 1</h3><div class='popover-content'>Incluir os dados da ajuda </div><div class='popover-navigation text-center'><button class='btn btn-default' data-role='prev'>« Anterior</button><span data-role='separator'>&nbsp;</span><button class='btn btn-default' data-role='next'>Próximo »</button></div><div class='popover-navigation'><div class=' text-center'><button class='btn btn-primary' data-role='end'>Finalizar</button></div></div></div>",
              steps: [
              {
                element: "#form-cadastro",
                content: "<p>Dentro deste formulário, você colocará os dados do usuário",
                title: "titulo da ajuda",
                placement: "top"
              },
              {
                element: "#form-table",
                content: "<p>Nesta etapa será definida os dados do usuário</p>",
                title: "titulo da ajuda 2",
                placement: "top"
              }
            ]});

            tour.init();

            $("#bt-tour").click(function(){
                // Start the tour
                tour.start(true);    
            });
            

            $('.date-picker').datepicker({
                                orientation: "left",
                                autoclose: true,
                                showOnFocus: false,
                                language: "pt-BR"
                                });

            $('.date-picker input').on('blur', function(){
                                    var currVal = $(this).val();
                                    var isOK = App.checkDateIsOK(currVal);

                                    if (!isOK)
                                        $(this).val("");
                                    
                                });

            $(".date-picker input").inputmask("d/m/y", {showMaskOnHover : false});
            $("#cpf").inputmask("999.999.999-99", {placeholder:"_", showMaskOnHover : false});
            $(".mask-telefone").inputmask("+55 (99) 9999-9999", {showMaskOnHover : false});
            $("#cnpj").inputmask("99.999.999/9999-99", {showMaskOnHover : false});
            $("#ramal").inputmask("99999", {showMaskOnHover : false});
            $("#cep").inputmask("99999-999", {showMaskOnHover : false});
            $(".numero").inputmask("99999999", {showMaskOnHover : false});

            $(".mask-telefone").on("keydown change blur", function(e) {
                var isNumber = (((e.keyCode >= 48) && (e.keyCode <= 57)) || ((e.keyCode >= 96) && (e.keyCode <= 105)));
                var isTab = (e.keyCode == 9);
                var isRemoveKey = ((e.keyCode == 8) || (e.keyCode == 46));

                if ((!isTab && isNumber) || isRemoveKey){
                    if ($(this).val().replace(/[_\-()]/g,"").length >= 15){
                        $(this).inputmask("+55 (99) 99999-9999", {showMaskOnHover : false});
                    }else {
                        $(this).inputmask("+55 (99) 9999-9999", {showMaskOnHover : false});
                    }
                }
            });

        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});