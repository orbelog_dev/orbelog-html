var FormValidation = function () {

    // basic validation
    var handleValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form = $('#form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: ":hidden",  // validate all fields including form hidden input
                rules: {
                    tipotac: {
                        required: true
                    },
                    filial: {
                        required: true
                    },
                    diaRecolhimento: {
                        required: true
                    },
                    vigencia: {
                        required: true
                    },
                    de1: {
                        required: true
                    },
                    ate1:{
                        required:true
                    },
                    aliquota1:{
                        required:true
                    },
                    deduzir1:{
                        required:true
                    },
                    de2: {
                        required: true
                    },
                    ate2:{
                        required:true
                    },
                    aliquota2:{
                        required:true
                    },
                    deduzir2:{
                        required:true
                    },
                    de3: {
                        required: true
                    },
                    ate3:{
                        required:true
                    },
                    aliquota3:{
                        required:true
                    },
                    deduzir3:{
                        required:true
                    },
                    de4: {
                        required: true
                    },
                    ate4:{
                        required:true
                    },
                    aliquota4:{
                        required:true
                    },
                    deduzir4:{
                        required:true
                    },
                    de5: {
                        required: true
                    },
                    ate5:{
                        required:true
                    },
                    aliquota5:{
                        required:true
                    },
                    deduzir5:{
                        required:true
                    },
                    de6: {
                        required: true
                    },
                    ate6:{
                        required:true
                    },
                    aliquota6:{
                        required:true
                    },
                    deduzir6:{
                        required:true
                    }

                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    
                },

                highlight: function (element) { // hightlight error inputs
                    $("#" + $(element).data('label')).addClass('has-error');
                    $(element).addClass('has-error');
                    $(element).parent().find('.chosen-single').addClass('has-error');
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $("#" + $(element).data('label')).removeClass('has-error');
                    $(element).removeClass('has-error');
                    $(element).parent().find('.chosen-single').removeClass('has-error');
                },

                success: function (label) {
                    
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    App.scrollTo(error, -200);
                }
            });

    }

    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"],
                "html" : false,
                
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {

            handleWysihtml5();
            handleValidation();
            
            $("#btn-cancelar").click(function(){
                window.location.href = "list-irrf.html";
            });     

            $("#cmbFilial").on("select2:select", function(){
                $(this).closest("form").submit();
            });


            $(".chosen-select").chosen();

            // Instance the tour
            var tour = new Tour({
              backdrop: true,
              template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'>Titulo do passo 1</h3><div class='popover-content'>Incluir os dados da ajuda </div><div class='popover-navigation text-center'><button class='btn btn-default' data-role='prev'>« Anterior</button><span data-role='separator'>&nbsp;</span><button class='btn btn-default' data-role='next'>Próximo »</button></div><div class='popover-navigation'><div class=' text-center'><button class='btn btn-primary' data-role='end'>Finalizar</button></div></div></div>",
              steps: [
              {
                element: "#form-cadastro",
                content: "<p>Dentro deste formulário, você colocará os dados do usuário",
                title: "titulo da ajuda",
                placement: "top"
              },
              {
                element: "#form-table",
                content: "<p>Nesta etapa será definida os dados do usuário</p>",
                title: "titulo da ajuda 2",
                placement: "top"
              }
            ]});

            tour.init();

            $("#bt-tour").click(function(){
                // Start the tour
                tour.start(true);    
            });
            function verificaChecks() {
                var aChk = document.getElementsByName("chk1"); 
                    if (aChk.checked == true){ 
                        // CheckBox Marcado... Faça alguma coisa... Ex:
                        alert(" marcado.");
                    }  else {
                        alert("deu ruim");
                    }
            }

            $('.date-picker').datepicker({
                                orientation: "left",
                                autoclose: true,
                                showOnFocus: false,
                                language: "pt-BR"
                                });

            $('.date-picker input').on('blur', function(){
                                    var currVal = $(this).val();
                                    var isOK = App.checkDateIsOK(currVal);

                                    if (!isOK)
                                        $(this).val("");
                                    
                                });

            $(".date-picker input").inputmask("d/m/y", {showMaskOnHover : false});
            $(".dia").inputmask("d", {showMaskOnHover : false});
            $("#cpf").inputmask("999.999.999-99", {placeholder:"_", showMaskOnHover : false});
            $("#codigo").inputmask("999999999", {placeholder:"_", showMaskOnHover : false});
            $(".mask-telefone").inputmask("+55 (99) 9999-9999", {showMaskOnHover : false});
            $(".mask-cnpj").inputmask("99.999.999/9999-99", {showMaskOnHover : false});
            $(".mask-money").inputmask('decimal', {
                        'alias': 'numeric',
                        'groupSeparator': '.',
                        'autoGroup': true,
                        'digits': 2,
                        'radixPoint': ",",
                        'digitsOptional': false,
                        'allowMinus': false,
                        'rightAlign': false,
                        'prefix': 'R$ ',
                        'showMaskOnHover': false,
                        'placeholder': ''
            });  
            $(".mask-valor").inputmask('decimal', {
                        'alias': 'numeric',
                        'autoGroup': true,
                        'digits': 2,
                        'radixPoint': ",",
                        'digitsOptional': false,
                        'allowMinus': false,
                        'rightAlign': false,
                        'integerDigits': 2,
                        'showMaskOnHover': false,
                        'placeholder': ''
            }); 

            $(document).ready(function () {

               $("#chk1").click(function () {  
                  if( $("#chk1").is(':checked') ){
                        $("#faixa1").show();
                        $("#de1").show(); 
                        $("#ate1").show();
                        $("#aliquota1").show();
                        $("#deduzir1").show();                   
                  } else {
                        $("#faixa1").hide();
                        $("#de1").hide();
                        $("#ate1").hide();
                        $("#aliquota1").hide();
                        $("#deduzir1").hide();
                  }
              
               });

             
            });
            $(document).ready(function () {

               $("#chk2").click(function () {  
                  if( $("#chk2").is(':checked') ){
                        $("#faixa2").show();
                        $("#de2").show(); 
                        $("#ate2").show();
                        $("#aliquota2").show();
                        $("#deduzir2").show();                   
                  } else {
                        $("#faixa2").hide();
                        $("#de2").hide();
                        $("#ate2").hide();
                        $("#aliquota2").hide();
                        $("#deduzir2").hide();
                  }
              
               });

             
            });

            $(document).ready(function () {

               $("#chk3").click(function () {  
                  if( $("#chk3").is(':checked') ){
                        $("#faixa3").show();
                        $("#de3").show(); 
                        $("#ate3").show();
                        $("#aliquota3").show();
                        $("#deduzir3").show();                   
                  } else {
                        $("#faixa3").hide();
                        $("#de3").hide();
                        $("#ate3").hide();
                        $("#aliquota3").hide();
                        $("#deduzir3").hide();
                  }
              
               });

             
            });

            $(document).ready(function () {

               $("#chk4").click(function () {  
                  if( $("#chk4").is(':checked') ){
                        $("#faixa4").show();
                        $("#de4").show(); 
                        $("#ate4").show();
                        $("#aliquota4").show();
                        $("#deduzir4").show();                   
                  } else {
                        $("#faixa4").hide();
                        $("#de4").hide();
                        $("#ate4").hide();
                        $("#aliquota4").hide();
                        $("#deduzir4").hide();
                  }
              
               });

             
            });

            $(document).ready(function () {

               $("#chk5").click(function () {  
                  if( $("#chk5").is(':checked') ){
                        $("#faixa5").show();
                        $("#de5").show(); 
                        $("#ate5").show();
                        $("#aliquota5").show();
                        $("#deduzir5").show();                   
                  } else {
                        $("#faixa5").hide();
                        $("#de5").hide();
                        $("#ate5").hide();
                        $("#aliquota5").hide();
                        $("#deduzir5").hide();
                  }
              
               });

             
            });
            $(document).ready(function () {

               $("#chk6").click(function () {  
                  if( $("#chk6").is(':checked') ){
                        $("#faixa6").show();
                        $("#de6").show(); 
                        $("#ate6").show();
                        $("#aliquota6").show();
                        $("#deduzir6").show();                   
                  } else {
                        $("#faixa6").hide();
                        $("#de6").hide();
                        $("#ate6").hide();
                        $("#aliquota6").hide();
                        $("#deduzir6").hide();
                  }
              
               });

             
            });

        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});