var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);


            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    filial: {
                        required: true
                    },
                    tipoveiculo:{
                        required:true
                    },
                    carroceria:{
                        required:true
                    },
                    pesoveiculo:{
                        required:true
                    },
                    cubagemMaxima:{
                        required:true
                    },
                    pesoMaximo:{
                        required:true
                    },
                    pesoCarga:{
                        required:true
                    },
                    cubagemFrete:{
                        required:true
                    },
                    quantidadeVeiculo:{
                        required:true
                    },
                    valorFrete:{
                        required:true
                    },
                    valorTotal:{
                        required:true
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type

                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $("#" + $(element).data('label')).addClass('has-error');
                    $(element).addClass('has-error');
                    $(element).parent().find('.chosen-single').addClass('has-error');
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $("#" + $(element).data('label')).removeClass('has-error');
                    $(element).removeClass('has-error');
                    $(element).parent().find('.chosen-single').removeClass('has-error');
                },

                success: function (label) {

                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    form[0].submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab4 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function(){ 
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                    $('#form_wizard_1').find('.button-cancel').show();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                    $('#form_wizard_1').find('.button-cancel').hide();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.form-wizard'));
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                    
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#progress-bar-wizard').css({
                        width: $percent + '%'
                    });
                }
            });

            $(".button-cancel").click(function(e){
                window.location = "list-planejamento.html";
            });

            if(location.search == "?acao=editar"){
            var dados = location.search.split("?");
            var acao = dados[1];
            var valor = acao.replace("acao=", "");
            if (valor == "editar"){
                document.querySelector("[name='pesoveiculo']").value = 10;
                document.querySelector("[name='quantidadeveiculo']").value = 3;
                document.querySelector("[name='filial']").value = 1;
                document.querySelector("[name='status']").value = 1;
                document.querySelector("[name='tipoveiculo']").value = 1;
                document.querySelector("[name='carroceria']").value = 1;
                document.querySelector("[name='carroceria']").value = 1;
                document.querySelector("[name='precofrete']").value = 175.55;
                document.querySelector("[name='valoracertado']").value = 1;
            };
        }

            $('.date-picker').datepicker({
                                orientation: "left",
                                autoclose: true,
                                showOnFocus: false,
                                language: "pt-BR"
            });

            $('.date-picker input').on('blur', function(){
                                    var currVal = $(this).val();
                                    var isOK = App.checkDateIsOK(currVal);

                                    if (!isOK)
                                        $(this).val("");
            });

            $(".date-picker input").inputmask("d/m/y", {showMaskOnHover : false});
            $(".hora").inputmask("h:s",{ showMaskOnHover: false });
            $(".mask-money").inputmask('decimal', {
                        'alias': 'numeric',
                        'groupSeparator': '.',
                        'autoGroup': true,
                        'digits': 2,
                        'radixPoint': ",",
                        'digitsOptional': false,
                        'allowMinus': false,
                        'rightAlign': false,
                        'integerDigits':20,
                        'prefix': 'R$ ',
                        'showMaskOnHover': false,
                        'placeholder': ''
            });  
            $(".mask-valor").inputmask('decimal', {
                        'alias': 'numeric',
                        'autoGroup': true,
                        'digits': 2,
                        'radixPoint': ",",
                        'digitsOptional': false,
                        'allowMinus': false,
                        'rightAlign': false,
                        'integerDigits': 2,
                        'showMaskOnHover': false,
                        'placeholder': ''
            });   
            $(".mask-peso").inputmask('decimal', {
                        'alias': 'numeric',
                        'autoGroup': true,
                        'digits': 3,
                        'radixPoint': ",",
                        'digitsOptional': false,
                        'allowMinus': false,
                        'rightAlign': false,
                        'integerDigits': 3,
                        'showMaskOnHover': false,
                        'placeholder': ''
            });          
            $(".numero").inputmask("9{0,9}", {showMaskOnHover : false});
            $(document).ready(function() {
                $('input:radio[name=valueRadio]').change(function() {
                    if (this.value == 'nao') {
                        $("#valorFrete").hide();
                    }
                    else if (this.value == 'sim') {
                        $("#valorFrete").show();
                    }
                });
            });
            $("#pesoveiculo").inputmask("decimal",{integerDigits: 3, digits: 3, showMaskOnHover: false, rightAlign: false});
            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                $('#validator');
                swal({
                    title: 'Sucesso',
                    text: 'Planejamento confirmado com sucesso!',
                    type: 'success'
                },function(){
                                window.location = "list-planejamento.html";
                            });
            }).hide();
        }

    };

}();

var IniciandoComboBox = function () {
    return {
        
        init: function () {
            
            //Inicializando o combobox de Tipo de Veículo    
            $.ajax({
                url: "data/tipo-veiculo.json",
                success: function(obj){
                    $("#form_tipo_veiculo option").remove();
                    
                    $.each(obj.data, function(i, elem){
                        $("#form_tipo_veiculo").append("<option data-image='"+ elem.img +"' value='"+ elem.id +"'>"+ elem.nome +"</option>");                        
                    });

                    $(".chosen-select").chosen();

                    $("#form_tipo_veiculo").chosen().change(function(){
                        var caminhoImagem = $(this).find("option:selected").data('image');
                        $("#imgTipoVeiculo").attr("src", caminhoImagem).fadeIn('fast');
                    });

                },
                error: function(err){
                    console.log(err);
                }
            })

        }

    };

}();

var IniciandoAcoesGrid = function(){

    var toneladasTotais = 10;
    var toneladasSelecionadas = 0;

    var atualizaGauge = function(){
        var percentualAtual = ((toneladasSelecionadas / toneladasTotais) * 85);
        if (percentualAtual > 85){
            percentualAtual = 85;
        }
        var percentualExcedido = percentualAtual-85;

        $("#gaugePrincipal").css({ width : percentualAtual + '%' });
        $("#gaugeExcedido").css({ width : percentualExcedido + '%' });
    }

    var inicializaCheckboxes = function(){
        $("#tblPedidos .checkboxes").click(function(){
            var checked = $(this).is(":checked");
            var $tr = $(this).parent().parent().parent();

            if (checked){
                var toneladas = $tr.find(".peso").html().replace("T", "");
                toneladasSelecionadas = toneladasSelecionadas + parseInt(toneladas);
            }else{
                var toneladas = $tr.find(".peso").html().replace("T", "");
                toneladasSelecionadas = toneladasSelecionadas - parseInt(toneladas);
            }   

            atualizaGauge();
        });
    }

    return {
        init: function(){
            inicializaCheckboxes();
        }
    }


}();


jQuery(document).ready(function() {
    FormWizard.init();
    IniciandoComboBox.init();
    IniciandoAcoesGrid.init();
});





