
if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
		
		$("#btn-cancelar").click(function(){
			window.location.href = "list-planejamento.html";
		});
		
		$(".chosen-select").chosen();

        
        $("#radioNaoVincularTAC").click(function(){
            $("#gridTACs").fadeOut();
        });

        $("#radioVincularTAC").click(function(){
            $("#gridTACs").fadeIn();
        });

    });
}