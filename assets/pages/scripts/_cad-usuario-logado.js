var FormValidation = function () {

    // basic validation
    var handleValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form = $('#form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    cpf: {
                        minlength: 8,
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                }
            });

    }

    return {
        //main function to initiate the module
        init: function () {

            handleValidation();
            
            $("#btn-cancelar").click(function(){
                window.location.href = "list-usuario.html";
            });     

            $('.date-picker').datepicker({
                orientation: "left",
                autoclose: true,
                showOnFocus: false,
                language: "pt-BR"
            });

            $("#dt_nascimento").inputmask("d/m/y");
            $("#cpf").inputmask("999.999.999-99", {placeholder:"_"});
            
            $("#telCelular").on("keydown change blur", function(e) {
                var isNumber = (((e.keyCode >= 48) && (e.keyCode <= 57)) || ((e.keyCode >= 96) && (e.keyCode <= 105)));
                var isTab = (e.keyCode == 9);
                var isRemoveKey = ((e.keyCode == 8) || (e.keyCode == 46));

                if ((!isTab && isNumber) || isRemoveKey){
                    if ($(this).val().replace(/[_\-()]/g,"").length >= 15){
                        $(this).inputmask("+55 (99) 99999-9999");
                    }else {
                        $(this).inputmask("+55 (99) 9999-9999");
                    }
                }
            });

            if ($("#telCelular").val().replace(/[_\-()]/g,"").length >= 11){
                $("#telCelular").inputmask("+55 (99) 99999-9999");
            }else {
                $("#telCelular").inputmask("+55 (99) 9999-9999");
            }
            
            $("#telFixo").inputmask("+55 (99) 9999-9999");
            
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});