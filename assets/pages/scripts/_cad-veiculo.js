var FormValidation = function () {

    // basic validation
    var handleValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form = $('#form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    placa: {
                        required: true
                    },
                    chassi: {
                        required: true
                    },
                    marca: {
                        required: true
                    },
                    modelo: {
                        required: true
                    },
                    cor: {
                        required: true
                    },
                    tracionador: {
                        required: true
                    },
                    tipoveiculo:{
                        required: true
                    },
                    uf:{
                        required: true
                    },
                    cidade:{
                        required: true
                    },
                    anofabricacao:{
                        required: true
                    },
                    anomodelo: {
                        required: true
                    },
                    tipocarga: {
                        required: true
                    },
                    carroceria: {
                        required: true
                    },
                    classificacao: {
                        required: true
                    },
                    tipotracao: {
                        required: true
                    },
                    combustivel: {
                        required: true
                    },
                    tipoproprietario: {
                        required: true
                    },
                    nomeproprietario: {
                        required: true
                    },
                    cpf: {
                        required: true
                    },
                    dataaquisicao: {
                        required: true
                    },
                    status: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    
                },

                highlight: function (element) { // hightlight error inputs
                    $("#" + $(element).data('label')).addClass('has-error');
                    $(element).addClass('has-error');
                    $(element).parent().find('.chosen-single').addClass('has-error');
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $("#" + $(element).data('label')).removeClass('has-error');
                    $(element).removeClass('has-error');
                    $(element).parent().find('.chosen-single').removeClass('has-error');
                },

                success: function (label) {
                    
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });


    }

    return {
        //main function to initiate the module
        init: function () {

            handleValidation();
            
            $("#btn-cancelar").click(function(){
                window.location.href = "list-veiculo.html";
            });     

            
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();

    $(".chosen-select").chosen();

    $('.date-picker').datepicker({
                    orientation: "left",
                    autoclose: true,
                    showOnFocus: false,
                    language: "pt-BR"
                });

    $.getJSON( "data/marcas.json", function( data ) {
        $("#comboMarcas option").remove();
        $.each(data, function(index, val){
            $("#comboMarcas").append("<option value='"+ val.idMarca +"'>"+ val.nomeMarca +"</option>");
        });
        $('#comboMarcas').trigger("chosen:updated");
    });


    $('#comboMarcas').on('change', function(evt, valSelected) {

        $.getJSON( "data/modelos.json", function( data ) {
            $("#comboModelos option").remove();
            
            $.each(data, function(index, val){
                if (val.idMarca == valSelected.selected){
                    $("#comboModelos").append("<option value='"+ val.idModelo +"'>"+ val.nomeModelo +"</option>");
                }
            });
            
            $('#comboModelos').trigger("chosen:updated");
        });                    
    });

    $('.date-picker').datepicker({
                        orientation: "left",
                        autoclose: true,
                        showOnFocus: false,
                        language: "pt-BR"
                        });

    $('.date-picker input').on('blur', function(){
                            var currVal = $(this).val();
                            var isOK = App.checkDateIsOK(currVal);

                            if (!isOK)
                                $(this).val("");
                            
                        });

    $(".date-picker input").inputmask("d/m/y", {showMaskOnHover : false});
    $(".date-year").inputmask("y", {showMaskOnHover : false});
    $("#placa").inputmask("aaa-9999");
    $(".ano").inputmask("9999");
    $("#cpf").inputmask("999.999.999-99", {placeholder:"_", showMaskOnHover : false});
    $(".litragem").inputmask("999999",  {showMaskOnHover : false});
    $("#renavam").inputmask("99999999999",  {showMaskOnHover : false});
    $("#chassi").inputmask("**************",  {showMaskOnHover : false});
    $(document).ready(function() {
        $('input:radio[name=tipoproprietario]').change(function() {
            if (this.value == 'fisica') {
                $("#cpf").inputmask("999.999.999-99", {placeholder:"_", showMaskOnHover : false});
                document.getElementById("lb-cpf").innerText = "CPF: *";
            }
            else if (this.value == 'juridica') {
                $("#cpf").inputmask("99.999.999/9999-99", {showMaskOnHover : false});
                document.getElementById("lb-cpf").innerText = "CNPJ: *";
            }
        });
    });


});