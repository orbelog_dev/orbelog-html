var Dashboard = function() {

    return {

        initSelectChosen: function(){
            $(".chosen-select").chosen();
        },

        initEasyPieCharts: function() {
            if (!jQuery().easyPieChart) {
                return;
            }

            $('.easy-pie-chart .number.transactions').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('yellow')
            });

            $('.easy-pie-chart .number.visits').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('green')
            });

            $('.easy-pie-chart .number.bounce').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('red')
            });

            $('.easy-pie-chart-reload').click(function() {
                $('.easy-pie-chart .number').each(function() {
                    var newValue = Math.floor(100 * Math.random());
                    $(this).data('easyPieChart').update(newValue);
                    $('span', this).text(newValue);
                });
            });
        },

        initHighCharts: function(){
            // Build the chart
            $('#graphCaptacao').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    height: 120,
                    spacingTop: 0,
                    spacingLeft: 0,
                    spacingRight: 0,
                    spacingBottom: 0,
                    x:0
                },
                title: {
                    text: 'CAPTAÇÕES',
                    floating: true,
                    x:-50,
                    y:25,
                    align: 'right',
                    style: {'font-family': 'Open Sans', 'fontWeight' : 'normal', 'margin-left': '100px'}
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                legend: {
                    layout: "vertical",
                    verticalAlign: "middle",
                    x: 50,
                    y: 5
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true,
                        center: ['50px', '50%']
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Pendentes',
                        y: 56.33
                    }, {
                        name: 'Finalizadas',
                        y: 24.03
                    }]
                }],
                responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }]
                }
            });

            // Build the chart
            $('#graphTacs').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    height: 120,
                    spacingTop: 0,
                    spacingLeft: 0,
                    spacingRight: 0,
                    spacingBottom: 0,
                    x:0
                },
                title: {
                    text: 'TACS',
                    floating: true,
                    align: 'right',
                    x:-120,
                    y:25,
                    style: {'font-family': 'Open Sans', 'fontWeight' : 'normal', 'margin-left': '100px'}
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                legend: {
                    layout: "vertical",
                    verticalAlign: "middle",
                    x: 50,
                    y: 15
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true,
                        center: ['50px', '50%']
                    }
                },
                series: [{
                    name: 'Quantidade',
                    colorByPoint: true,
                    data: [{
                        name: 'Pendentes',
                        y: 45,
                        color: "#2E9947"
                    }, {
                        name: 'Em viagem',
                        y: 15,
                        color: "#C49F47"
                    }, {
                        name: 'Finalizadas',
                        y: 40,
                        color: "#7CB5EC"
                    }]
                }],
                responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }]
                }
            });



            $.ajax({
                url: "data/acompanhamento.json",
                dataType: "json",
                success: function(data){
                    
                    console.log(data);
                    
                    $('#highchart').highcharts({
                        chart : {
                            type: 'column'
                        },
                        title: {
                            text: 'Acompanhamento de Entregas',
                            x: -20 //center
                        },
                        subtitle: {
                            text: '(Atualizado em 19/10/2016 15:15)',
                            x: -20
                        },
                        xAxis: {
                            categories: data.xAxis.data
                        },
                        yAxis: [
                            {
                                min:0,
                                title: {
                                    text: 'Viagens do dia'
                                }
                            }
                        ],
                        tooltip: {
                            shared: true
                        },
                        plotOptions:{
                            column:{
                                grouping: false,
                                shadow: false,
                                borderWidth: 0
                            }
                        },
                        series: [data.serie1, 
                                 data.serie2]
                    });
                },
                error: function(err){
                    console.log(err);
                },
                complete: function(){

                }
            });



        },

        initDatePicker: function(){
            $(".date-picker").datepicker();
        },

        
        init: function() {
            this.initEasyPieCharts();
            this.initSelectChosen();
            this.initHighCharts();
            this.initDatePicker();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {
        Dashboard.init(); // init metronic core componets
    });
}