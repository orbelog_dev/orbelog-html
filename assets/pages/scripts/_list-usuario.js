var TableDatatablesManaged = function () {

    var table = "";

    var initTable1 = function () {
        table = $('#sample_1');
        var btnExportar =   '<div class="btn-group">'+
                            '    <button class="btn green  dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Exportar'+
                            '        <i class="fa fa-angle-down"></i>'+
                            '    </button>'+
                            '    <ul class="dropdown-menu pull-right">'+
                            '        <li>'+
                            '            <a href="javascript:;">'+
                            '                <i class="fa fa-file-pdf-o"></i> Exportar para PDF </a>'+
                            '        </li>'+
                            '        <li>'+
                            '            <a href="javascript:;">'+
                            '                <i class="fa fa-file-excel-o"></i> Exportar para Excel </a>'+
                            '        </li>'+
                            '    </ul>'+
                            '</div>';

        var btnAcoes =  '<div class="btn-group">' +
                        '    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações' +
                        '        <i class="fa fa-angle-down"></i>' +
                        '    </button>' +
                        '   <ul class="dropdown-menu pull-right" role="menu">' +
                        '        <li>' +
                        '            <a href="javascript:;">' +
                        '                <i class="fa fa-envelope"></i> Reenviar Senha </a>' +
                        '        </li>' +
                        '       <li>' +
                        '           <a href="cad-usuario.html">' +
                        '                <i class="fa fa-pencil"></i> Editar / Visualizar </a>' +
                        '        </li>' +
                        '        <li>' +
                        '            <a href="javascript:;">' +
                        '                <i class="fa fa-toggle-on"></i> Inativar </a>' +
                        '        </li>' +
                        '        <li>' +
                        '            <a href="javascript:;">' +
                        '                <i class="fa fa-remove"></i> Excluir' +
                        '            </a>' +
                        '        </li>' +
                        '    </ul>' +
                        '</div>';

        // begin first table
        table.dataTable({
            "ajax": "data/usuarios.json",
            "processing": true,
            "responsive": true,
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": ative para ordenar a coluna de forma crescente",
                    "sortDescending": ": ative para ordenar a coluna de forma decrescente"
                },
                "emptyTable": "Não tem nenhuma informação para ser exibida",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhuma informação retornada",
                "infoFiltered": "(filtrado de _MAX_ registros)",
                "lengthMenu": "Exibir _MENU_",
                "search": "Busca:",
                "zeroRecords": "Nenhum registro encontrado",
                "paginate": {
                    "previous":"Anterior",
                    "next": "Próximo",
                    "last": "Último",
                    "first": "Primeiro"
                }
            },

            "initComplete": function( settings, json ) {
                $(".btn-actions").html(btnExportar);

                $(".dataTables_filter label input").focus(function(){
                    $(this).removeClass('input-small').animate({width : "250px"}, 2000, 'easeOutElastic');
                });

                App.scrollTo($("#ancoraPesquisa"), -50);
            },

            "drawCallback": function( settings ) {
                $(".lnk-editar").click(function(e){
                    window.location = "#";
                })
                $(".lnk-desativar").click(function(e){
                    window.location = "#";
                })
            },
            
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'<'btn-actions pull-right'><'pull-right'f>>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 15, 20],
                [10, 15, 20] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,            
            "pagingType": "bootstrap_full_number",

            "columnDefs": [
                {  // set default column settings
                    "width": "5%",
                    "data": "status",
                    "className": "dt-center",
                    "targets": [0]
                },
                {  // set default column settings
                    "width": "25%",
                    "data": "nome",
                    "responsivePriority": 3,
                    'targets': [1]
                },
                {  // set default column settings
                    "width": "20%",
                    "data": "sobrenome",
                    "responsivePriority": 3,
                    'targets': [2]
                },
                {  // set default column settings
                    "width": "10%",
                    "data": "email",
                    "responsivePriority": 3,
                    'targets': [3]
                },
                {  // set default column settings
                    "width": "10%",
                    "data": "dt_vigencia",
                    "responsivePriority": 3,
                    'targets': [4]
                },
                {
                    "width": "5%",
                    "searchable": false,
                    "orderable": false,
                    "targets": [5],
                    "data" : null,
                    "responsivePriority": 1,
                    "defaultContent" : btnAcoes
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

    }

    var refreshTable = function(){
        App.scrollTo($("#ancoraPesquisa"), -50);
    }

    var isInitialized = function(){
        return (table != "")
    }


    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            if (isInitialized())
                refreshTable();    
            else
                initTable1();              
            
        }

    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        
        $("#title-collapse").click(function(e){
            var $this = $(this);
            var target = $('#portlet-body-search');
            var $icon = $("#icon-collapse");

            target.slideToggle(1000, 'easeOutCubic', function(){
                if (target.is(":visible")){
                    $icon.removeClass("fa-chevron-up").addClass("fa-chevron-down");
                }else{
                    $icon.removeClass("fa-chevron-down").addClass("fa-chevron-up");
                }
            });
        });

        $("#icon-collapse").click(function(e){
            var $this = $(this);
            var target = $('#portlet-body-search');
            
            target.slideToggle(1000, 'easeOutCubic', function(){
                if (target.is(":visible")){
                    $this.removeClass("fa-chevron-up").addClass("fa-chevron-down");
                }else{
                    $this.removeClass("fa-chevron-down").addClass("fa-chevron-up");
                }
            });
        });
        
        $(".chosen-select").chosen();
        $(".chosen-select.responsiveChosen").chosen({width:"100%"});

        $("#btn-pesquisa").click(function(){
            $("#pnPesquisa").show();
            TableDatatablesManaged.init();
        });
    });
}