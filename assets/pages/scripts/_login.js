var Login = function() {

    var handleLogin = function() {

        $('#login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                email: {
                    required: true
                },
                password: {
                    required: true
                }
            },

            messages: {
                username: {
                    required: "E-mail obrigatório"
                },
                password: {
                    required: "Senha obrigatória"
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element).addClass('has-error');
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element).removeClass('has-error');
            },

            success: function(element) {
                $(element).removeClass('has-error');  
            },

            errorPlacement: function(error, element) {
                
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });


        $('#login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#login-form').validate().form()) {
                    $('#login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });


    }

    return {
        //main function to initiate the module
        init: function() {
            handleLogin();
        }

    };

}();


var RememberLogin = function() {

    var handleRemember = function() {

        $('#forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                email: {
                    required: true
                }
            },

            messages: {
                email: {
                    required: "E-mail é de preenchimento obrigatório."
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('#forget-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element).addClass('has-error');
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element).removeClass('has-error');
            },

            success: function(element) {
                $(element).removeClass('has-error');  
            },


            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });


        $('#forget-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#forget-form').validate().form()) {
                    $('#forget-form').submit();
                }
                return false;
            }
        });

        $('#forget-password').click(function(){
            $('#login-form').hide();
            $('#forget-form').show();
            $(".alert-danger").hide();
        });

        $('#back-btn').click(function(){
            $('#login-form').show();
            $('#forget-form').hide();
            $(".alert-danger").hide();
        });
        

    }

    return {
        //main function to initiate the module
        init: function() {
            handleRemember();
        }

    };

}();


jQuery(document).ready(function() {
    Login.init();
    RememberLogin.init();

});